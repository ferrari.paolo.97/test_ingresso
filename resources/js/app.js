require('./bootstrap');

const app = new Vue({
    el: "#app",
    data: {
        risultato_fibonacci: null,
        eta: null,
        media: null,
        studenti: null
    },
    methods: {
        
    },
    mounted() {
        /* Esercizio 1 */
        let n1 = 1;
        let n2 = 1;
        let n3 = 2;
        /* n per contare posizione di n3 in sequenza di Fibonacci */
        let nc = 2;

        while (n3.toString().length < 10) { // cerco con 10 cifre (se voglio cambio 10 con 1000, però ci metterà un po' di tempo)
            n3 = n1 + n2;

            n1 = n2;
            n2 = n3;

            nc++;
        }

        this.risultato_fibonacci = nc;

        /* Esercizio 2 */
        var student = {
            firstname: "Mario",
            lastname: "Rossi",
            birthdate: "22-09-1974",
            grades: "5, 8, 9, 10 , 10, 3, 4.5, 6, 8, 2" //c'era scritto string perciò li ho inseriti come una stringa di testo
        };

        /* age */
        var data_splittata = student.birthdate.split("-");
        var d = new Date();
        var y = d.getFullYear();
        this.eta = y - data_splittata[2];

        /* avg_grade */
        var voti_splittati = student.grades.split(", ");
        var sommavoti = 0;
        var numerovoti = 0;
        for (let i = 0; i < voti_splittati.length; i++) {
            sommavoti += parseFloat(voti_splittati[i]);
            numerovoti++;
        }
        this.media = (sommavoti / numerovoti).toFixed(1);

        /* Esercizio 3 */
        axios.get('/api/students').then(resp => {
            this.studenti = resp.data;

        }).catch(e => {
            console.error('Sorry! ' + e);
        })
        
    }
});