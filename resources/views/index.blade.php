<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test d'ingresso</title>
</head>

<body>

    <div id="app">

        <h1>Test d'ingresso</h1>

        <!-- Esercizio 1 -->
        <h3>Fase 1:</h3>
        <span>Posizione n. con 10 cifre: @{{ risultato_fibonacci }}</span>

        <!-- Esercizio 2 -->
        <h3>Fase 2:</h3>
        <span>Calcolo età studente: @{{ eta }}</span> <br>
        <span>Calcolo media voti studente: @{{ media }}</span>

        <!-- Esercizio 3 -->
        <h3>Fase 3:</h3>
        <h4>Lista studenti: [GET] [Con Chiamata Axios]</h4>
        <div v-for="studente in studenti">
            <span>@{{ studente . firstname }} @{{ studente . lastname }}</span> <br>
            <span>Data di nascita: @{{ studente . birthdate }}</span> <br>
            <span>Voti: @{{ studente . grades }}</span>
        </div>
        <h4>Crea nuovo studente: [POST]</h4>
        <a href="{{ route('create') }}">Crea nuovo studente da qui!</a>

    </div>

    <!-- VueJS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <!-- CDN Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
        integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- MyScript -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
