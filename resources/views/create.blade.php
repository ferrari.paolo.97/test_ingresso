<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test d'ingresso</title>
</head>

<body>

    <div id="app">

        <h1>Test d'ingresso</h1>

        <!-- Esercizio 3 -->
        <h3>Crea nuovo studente:</h3>

        <form action="{{ route('students.store') }}" method="post">
            @csrf

            <div class="form-group">
                <label for="firstname">Nome</label>
                <input type="text" class="form-control" name="firstname" id="firstname"
                    aria-describedby="firstnameHelperr" placeholder="Aggiungi il nome dello studente" />
            </div>

            <div class="form-group">
                <label for="lastname">Cognome</label>
                <input type="text" class="form-control" name="lastname" id="lastname"
                    aria-describedby="lastnameHelperr" placeholder="Aggiungi il nome dello studente" />
            </div>

            <div class="form-group">
                <label for="birthdate">Data di nascita</label>
                <input type="text" class="form-control" name="birthdate" id="birthdate"
                    aria-describedby="birthdateHelperr" placeholder="Aggiungi la data di nascita dello studente" />
            </div>

            <div class="form-group">
                <label for="grades">Voti</label>
                <input type="text" class="form-control" name="grades" id="grades" aria-describedby="gradesHelperr"
                    placeholder="Aggiungi i voti dello studente" />
            </div>



            <button type="submit">
                <h5 class="m-0 p-1">Crea</h5>
            </button>
        </form>

    </div>

    <!-- VueJS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <!-- CDN Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
        integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- MyScript -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
