<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('create', compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();


        $newstudent = new Student;

        $newstudent->firstname = $data['firstname'];
        $newstudent->lastname = $data['lastname'];
        $newstudent->birthdate = $data['birthdate'];
        $newstudent->grades = $data['grades'];
        $newstudent->save();

        return redirect()->route('index');
    }
}